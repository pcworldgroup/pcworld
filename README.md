## Pc-world



**pc-world est projet  e-commerce d'objectif réaliser un site**

** **de vente en ligne developpé en symfony.****



### *Conception:*

- Réalisation de diagrammes de Use Case

- Réalisation d'un diagramme de classe

- Réalisation de maquettes (wireframe)

  

### *Techniques utilisés*

- Utilisation symfony 4.2 avec la stack complète (Doctrine, sécurité, twig)

- Application responsive bootstrap

- Webpack encore

- utilisation de Gitlab pour le travail de groupe

  

### *Version:*

Actuellement en version 1.0

Ajout d'améliorations  à venir:

- habillage de site

- évolution globale de design de site

- ajout fonctionnalité suivi GPS de commande

  

### *Équipe de développemnt:*

-Avrora@apprenante simplonLyon

-Benoit@apprenant simplonLyon

-Jorge@apprenante simplonLyon

-Salwa@apprenate simplonLyon

