<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\LinkCart;
use App\Entity\Products;
use App\Entity\User;
use App\Entity\Cart;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\LinkCartRepository;

class CartController extends AbstractController
{
    /**
     * @Route("/add-cart{product}", name="add_cart")
     */
    public function index(Products $product, ObjectManager $manager)
    {
        if(!$this->getUser()){
            return $this->redirectToRoute('login');
        }
        if (!$this->getUser()->getCart()) {
            $cart = new Cart();
            $cart->setUser($this->getUser());
            $manager->persist($cart);
        } else {
            $cart = $this->getUser()->getCart();
        }
        $found = false;
        foreach ($cart->getLinkCart() as $key => $value) {
            if ($value->getProduct() == $product) {
                $value->setQuant($value->getQuant() + 1);
                $found = true;
            }
        }
        if (!$found) {
            $linkCart = new LinkCart();
            $user = $this->getUser();
            $linkCart->setQuant(1);
            $linkCart->setProduct($product)->setPrice($product->getPrice());
            $cart->addLinkCart($linkCart);
            $manager->persist($linkCart);
        }
        $manager->flush();
        return $this->redirectToRoute('home');
    }
    /**
     * @Route ("/cart", name="cart")
     */
    public function cart()
    {
        return $this->render("cart/index.html.twig", [
            'cart' => $this->getUser()->getCart(),
        ]);
    }
    /**
     * @Route ("/delete-item{linkcart}", name="delete_item")
     */
    public function deleteLinkCart(LinkCart $linkcart, ObjectManager $manager)
    {
        $cart = $this->getUser()->getCart();
        $cart->removeLinkCart($linkcart);
        $manager->flush();
        return $this->redirectToRoute('cart');
    }
}
