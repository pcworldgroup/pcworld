<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Products;
use App\Form\ProductsType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\ProductsRepository;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', []);
    }
    /**
     * @Route ("/admin/products", name="products")
     */
    public function listProducts(ProductsRepository $prodrepo)
    {
        $products = $prodrepo->findAll();
        return $this->render("admin/list-products.html.twig", [
            "products" => $products,
        ]);
    }
    /**
     * @Route ("/admin/add-products{products}", name="add-products")
     */
    public function addProducts(Request $request, ObjectManager $manager, Products $products= null)
    {
        $verb = 'Modify';
        if(!$products) {
        $products = new Products();
        $verb = 'Add';
        }
        $form = $this->createForm(ProductsType::class, $products);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($products);
            $manager->flush();
            return $this->redirectToRoute("products");
        }
        return $this->render("admin/add-products.html.twig", [
            "form" => $form->createView(),
            "products" => $this->getUser(),
            'verb'=> $verb
        ]);
    }
    /**
     * @Route ("/admin/delete-products{products}", name="delete-products")
     */
    public function delete(ObjectManager $manager, Products $products = null)
    {
        $manager->remove($products);
        $manager->flush();
        return $this->redirectToRoute("products");
    }
}
