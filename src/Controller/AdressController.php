<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Adress;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\AdressType;

class AdressController extends AbstractController
{
    /**
     * @Route("/adress", name="adress")
     */
    public function index()
    {
        $user = $this->getUser();
        dump($user->getAdress());
        return $this->render('adress/index.html.twig', []);
    }

    /**
     * @Route("/add-adress{adress}", name="add_adress")
     */
    public function addAdress(Request $request, ObjectManager $manager, Adress $adress = null)
    {
        if(!$adress){
            $adress = new Adress();
        }
        $form = $this->createForm(AdressType::class, $adress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $adress->addUser($this->getUser());
            $manager->persist($adress);
            $manager->flush();
            return $this->redirectToRoute('profile');
        }
        return $this->render('adress/add-adress.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/remove-adress{adress}", name="remove_adress")
     */
    public function removeAdress(ObjectManager $manager, Adress $adress)
    {
        $manager->remove($adress);
        $manager->flush();
        return $this->redirectToRoute('profile');
    }
}
