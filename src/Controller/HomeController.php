<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\UserType;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Products;
use App\Repository\ProductsRepository;
use App\Entity\Commentary;
use App\Form\CommentaryType;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Cart;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProductsRepository $prodrepo, ObjectManager $manager)
    {

        return $this->render('home/index.html.twig', [
            'products' => $prodrepo->findAll()
        ]);
    }
    //filtrer les produits selon le brand(marque)
    /**
     * @Route("/search", name="search")
     */
    public function search(ProductsRepository $prodrepo, Request $request)
    {
        $request->getBasePath('search');
        //Récupérer sur la request le champ du formulaire avec un get(le name de l'input)
        $search = $prodrepo->findByBrand($request->get('search'));
        //Utiliser la variable obtenue pour faire un findBy en utilisant le repository
        return $this->render('home/index.html.twig', [
            'products' => $search
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $encoder,
        ObjectManager $manager
    ) {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $pass = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pass);
            $user->setRole('ROLE_USER');

            $manager->persist($user);
            $cart = new Cart();
            $user->setCart($cart);
            $manager->persist($cart);
            $manager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route ("/login", name = "login")
     */
    public function login(AuthenticationUtils $auth, ObjectManager $manager)
    {


        $error = $auth->getLastAuthenticationError();
        $username = $auth->getLastUsername();

        return $this->render('user/login.html.twig', [
            'error' => $error,
            'username' => $username
        ]);
        $this->redirectToRoute('home');
    }
    /**
     * @Route("/see-product/{product}", name="see-product")
     */
    public function seeProduct(Products $product, ObjectManager $manager, Request $request, Commentary $comment = null)
    {

        $comment = new Commentary();

        $form = $this->createForm(CommentaryType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setProduct($product);
            $comment->setUser($this->getUser());
            $comment->setDate(new \DateTime());

            $manager->persist($comment);
            $manager->flush();
        }

        return $this->render('home/see-product.html.twig', [
            'product' => $product,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/redirection_role", name="redirect_role")
     */
    public function loginRedirection()
    {
        $user = $this->getUser();
        if ($user->getRole() === 'ROLE_ADMIN') {
            return $this->redirectToRoute('products');
        } else {
            return $this->redirectToRoute('home');
        }
    }
}
