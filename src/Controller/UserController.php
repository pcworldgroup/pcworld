<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\ModifyUserType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\PasswordChangeType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function index()
    {
        $user= $this->getUser();
        return $this->render('user/index.html.twig', [
            'user' => $user,
            'cart'=> $user->getCart(),
        ]);
    }
    /**
     * @Route("/profile/modify-user", name="modify_user")
     */
    public function modify(Request $request, ObjectManager $manager, User $user =null)
    {
        $user=$this->getUser();
        $form = $this->createForm(ModifyUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute("profile");
        }
        return $this->render("user/modify-user.html.twig", [
            "form" => $form->createView(),
        ]);
    }
    /**
     * @Route("/profile/modify-user-password", name="modify_user_password")
     */
    public function modifyPassword(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager){
        $user = $this->getUser();
        $form = $this->createForm(PasswordChangeType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute("profile");
        }
        return $this->render("user/modify-user-password.html.twig", [
            "form" => $form->createView(),
        ]);
    }

}
