<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Commentary;
use App\Entity\Products;
use App\Form\CommentaryType;
use App\Form\ProductsType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductsRepository;
use Symfony\Component\HttpFoundation\Response;


class CommentaryController extends AbstractController
{
    /**
     * @Route("/commentary/{id}", name="remove_comment")
     */
    public function removeComment(Commentary $comment, ObjectManager $manager) {
        if (!$comment) {
            
        } else {
            if ($comment->getUser() != $this->getUser()) {
                return new Response('You don\'t have permission to delete this commentary', 401);
            }
        }
        $product = $comment->getProduct();
        $manager->remove($comment);
        $manager->flush();

        return $this->redirectToRoute("see-product", [
            "product" => $product->getId()
        ]);
    }

    /**
     * @Route("/modify/commentary/{id}", name="modify_comment")   
     */
    public function modifyComment(Commentary $comment, ObjectManager $manager, Request $request, ProductsRepository $repo) {
        
        if(!$comment) {
            $comment = new Commentary();
        } else {
            if($comment->getUser() != $this->getUser()) {
                return new Response('You don\'t have permission to modify this commentary', 401);
            }
        }
        
        $form = $this->createForm(CommentaryType::class, $comment);
        $form->handleRequest($request);

        $product = $repo->find($comment->getProduct());


        if($form->isSubmitted() && $form->isValid()) {
            $comment->setDate(new \DateTime());
            $comment->setUser($this->getUser());
            $manager->merge($comment);
            $manager->flush();

            return $this->redirectToRoute('see-product', ['product' => $product->getId()]);
        }

        return $this->render('home/see-product.html.twig', [
            'product' => $product, //$repo->find($comment->getProduct()),
            'comment' => $comment,
            'form' => $form->createView()
        ]);
    }
}