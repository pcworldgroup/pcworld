<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LinkCart", mappedBy="cart", orphanRemoval=true)
     */
    private $linkCart;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="cart", cascade={"persist", "remove"})
     */
    private $user;

    public function __construct()
    {
        $this->linkCart = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|LinkCart[]
     */
    public function getLinkCart(): Collection
    {
        return $this->linkCart;
    }

    public function addLinkCart(LinkCart $linkCart): self
    {
        if (!$this->linkCart->contains($linkCart)) {
            $this->linkCart[] = $linkCart;
            $linkCart->setCart($this);
        }

        return $this;
    }

    public function removeLinkCart(LinkCart $linkCart): self
    {
        if ($this->linkCart->contains($linkCart)) {
            $this->linkCart->removeElement($linkCart);
            // set the owning side to null (unless already changed)
            if ($linkCart->getCart() === $this) {
                $linkCart->setCart(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function getTotal()
    {
        $total = 0;
        foreach ($this->linkCart as $key => $value) {
            $total+= $value->getPrice() * $value->getQuant();
        }
        return $total;
    }
}
