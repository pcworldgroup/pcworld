<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use App\Entity\Products;
use App\Entity\Adress;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class AppFixtures extends Fixture
{
    private $faker;
    public const USER_REFERENCE = 'user';

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = Faker\Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ($x = 1; $x < 15; $x++) {
            $user = new User();
            $user->setName($this->faker->name);
            $user->setSurname($this->faker->userName);
            $user->setMobileNb($this->faker->phoneNumber);
            $user->setEmail($this->faker->email);
            $user->setPassword($this->passwordEncoder->encodePassword($user, '12345'));
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            $this->addReference(self::USER_REFERENCE . $x, $user);
        }

        $user = new User();
        $user->setName('admin');
        $user->setSurname('admin');
        $user->setMobileNb($this->faker->phoneNumber);
        $user->setEmail('admin@admin.co');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'admin'));
        $user->setRole('ROLE_ADMIN');
        $manager->persist($user);



        for ($x = 1; $x < 15; $x++) {
            $products = new Products();
            $products->setBrand($this->faker->name);
            $products->setSize($this->faker->randomDigitNotNull);
            $products->setStock($this->faker->randomDigitNotNull);
            $products->setCategory(null);
            $products->setPrice($this->faker->randomDigitNotNull);
            $products->setDescription($this->faker->realText(120));
            $manager->persist($products);

        }


        for ($x = 1; $x < 3; $x++) {
            $adress= new Adress();
            $adress->setCity($this->faker->city);
            $adress->setCountry($this->faker->country);
            $adress->setNumber($this->faker->randomDigitNotNull);
            $adress->setStreet($this->faker->streetAddress);
            $adress->setZipCode($this->faker->countryCode);

            $manager->persist($adress);
        }


        $category1 = new Category();
        $category1->setType("Desktop");
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setType("Laptop");
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setType("Keyboard");
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setType("Mouse");
        $manager->persist($category4);

        $category5 = new Category();
        $category5->setType("Screens");
        $manager->persist($category5);

        $category6 = new Category();
        $category6->setType("Printer");
        $manager->persist($category6);

        $category7 = new Category();
        $category7->setType("Webcam");
        $manager->persist($category7);

        $category8 = new Category();
        $category8->setType("Headphones");
        $manager->persist($category8);

        $manager->flush();
    }
}
